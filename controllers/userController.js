let User = require('../models/user')

/*
    Add User to mongoDB
*/
exports.addPostUser = (req, res) => {
    var name = req.body.name;
    var email = req.body.email;
    var password = req.body.password;

    console.log("name - "+name+" email - "+email+" password - "+password);

    User.create({
        "name": name,
        "email": email,
        "password": password,
    })
        .then(user => {
            res.redirect('/user/show');
        })
        .catch((err) => {
            console.log(err);
            res.redirect('/user/add');
        });
}

/*
Show add user page
*/
exports.addGetUser = (req, res) => {
    res.render("add.hbs");
}

/*
    Show User to table in HTML
*/
exports.showUser = (req, res) => {
    res.render("home.hbs");
}

/*
get data from DB
*/
exports.getUser = (req, res) => {
    User.find().then(result=>{
        res.send({
            status:"true",
            result:result,
        })
    }).catch(err=>{
        console.log(err);
        res.send({
            status:"false",
            result:err,
        })
    })
}