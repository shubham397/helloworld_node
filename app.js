const port = 1234;
const bodyParser = require('body-parser');
const hbs = require('hbs');
const express = require("express");
const mongoose = require('mongoose');

const users = require('./routes/user');

var app = express();
app.set('view engine', 'hbs');

mongoose.connect('mongodb://localhost:27017/first');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/user', users);

app.listen(port, () => {
    console.log(`server is running on port ${port}`);
});