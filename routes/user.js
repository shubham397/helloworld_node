const express = require("express");
const router = express.Router();

const {addPostUser,addGetUser,showUser,getUser} = require('../controllers/userController');

router.post('/add', addPostUser); //POST
router.get('/add',addGetUser);//GET
router.get('/show', showUser); //GET
router.get('/get',getUser);

module.exports = router;